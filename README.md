An webpage where we have an "secured" version of communication with the database and an insecure one that is prone to SQLinjection or bad Authentication that is activated 
by checking the Vulnerability checkbox.

Some example SQLinjections to use:

	-Get all Matches in one Query and check to see if vulnerability is exploitable:
	1' OR '1' = '1

	-Get the sensitive information that was supposed to be unreachable
	1' UNION SELECT Username,Password, NULL as Col3 FROM Secret_Table WHERE '1' = '1

Bad authentication can be seen by responses that the webpage returns after login attempts